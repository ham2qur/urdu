namespace WindowsApplication7
{
  partial class Form1
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
        this.textBox1 = new System.Windows.Forms.TextBox();
        this.rcMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
        this.undoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
        this.cutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        this.copyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        this.pasteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        this.chkUrdu = new System.Windows.Forms.CheckBox();
        this.menuStrip1 = new System.Windows.Forms.MenuStrip();
        this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
        this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
        this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.printPreviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
        this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.redoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
        this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
        this.selectAllToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.customizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.windowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.Background_color = new System.Windows.Forms.ToolStripMenuItem();
        this.AliceBlue = new System.Windows.Forms.ToolStripMenuItem();
        this.Cornflower = new System.Windows.Forms.ToolStripMenuItem();
        this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
        this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
        this.texBoxColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.color1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.color2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.color3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.color4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.color5ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.indexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
        this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.ScrnNew = new System.Windows.Forms.Button();
        this.ScrnOpen = new System.Windows.Forms.Button();
        this.button3 = new System.Windows.Forms.Button();
        this.button4 = new System.Windows.Forms.Button();
        this.button5 = new System.Windows.Forms.Button();
        this.button7 = new System.Windows.Forms.Button();
        this.button6 = new System.Windows.Forms.Button();
        this.button1 = new System.Windows.Forms.Button();
        this.statusStrip1 = new System.Windows.Forms.StatusStrip();
        this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
        this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
        this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
        this.toolStrip1 = new System.Windows.Forms.ToolStrip();
        this.printToolStripButton = new System.Windows.Forms.ToolStripButton();
        this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
        this.cutToolStripButton = new System.Windows.Forms.ToolStripButton();
        this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
        this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
        this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
        this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
        this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
        this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
        this.AlignL = new System.Windows.Forms.ToolStripButton();
        this.AlignC = new System.Windows.Forms.ToolStripButton();
        this.AlignR = new System.Windows.Forms.ToolStripButton();
        this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
        this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
        this.ZoomIn = new System.Windows.Forms.ToolStripButton();
        this.ZoomOut = new System.Windows.Forms.ToolStripButton();
        this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
        this.toolStripComboBox2 = new System.Windows.Forms.ToolStripComboBox();
        this.timer = new System.Windows.Forms.Timer(this.components);
        this.openWork = new System.Windows.Forms.OpenFileDialog();
        this.saveWork = new System.Windows.Forms.SaveFileDialog();
        this.rcMenu.SuspendLayout();
        this.menuStrip1.SuspendLayout();
        this.statusStrip1.SuspendLayout();
        this.toolStrip1.SuspendLayout();
        this.SuspendLayout();
        // 
        // textBox1
        // 
        this.textBox1.ContextMenuStrip = this.rcMenu;
        this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
        this.textBox1.Location = new System.Drawing.Point(12, 131);
        this.textBox1.Multiline = true;
        this.textBox1.Name = "textBox1";
        this.textBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
        this.textBox1.Size = new System.Drawing.Size(630, 358);
        this.textBox1.TabIndex = 3;
        this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
        this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
        this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
        this.textBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyUp);
        // 
        // rcMenu
        // 
        this.rcMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem1,
            this.redoToolStripMenuItem,
            this.toolStripSeparator14,
            this.cutToolStripMenuItem1,
            this.copyToolStripMenuItem1,
            this.pasteToolStripMenuItem1});
        this.rcMenu.Name = "rcMenu";
        this.rcMenu.Size = new System.Drawing.Size(113, 120);
        // 
        // undoToolStripMenuItem1
        // 
        this.undoToolStripMenuItem1.Name = "undoToolStripMenuItem1";
        this.undoToolStripMenuItem1.Size = new System.Drawing.Size(112, 22);
        this.undoToolStripMenuItem1.Text = "Undo";
        // 
        // redoToolStripMenuItem
        // 
        this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
        this.redoToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
        this.redoToolStripMenuItem.Text = "Redo";
        // 
        // toolStripSeparator14
        // 
        this.toolStripSeparator14.Name = "toolStripSeparator14";
        this.toolStripSeparator14.Size = new System.Drawing.Size(109, 6);
        // 
        // cutToolStripMenuItem1
        // 
        this.cutToolStripMenuItem1.Name = "cutToolStripMenuItem1";
        this.cutToolStripMenuItem1.Size = new System.Drawing.Size(112, 22);
        this.cutToolStripMenuItem1.Text = "Cut";
        // 
        // copyToolStripMenuItem1
        // 
        this.copyToolStripMenuItem1.Name = "copyToolStripMenuItem1";
        this.copyToolStripMenuItem1.Size = new System.Drawing.Size(112, 22);
        this.copyToolStripMenuItem1.Text = "Copy";
        // 
        // pasteToolStripMenuItem1
        // 
        this.pasteToolStripMenuItem1.Name = "pasteToolStripMenuItem1";
        this.pasteToolStripMenuItem1.Size = new System.Drawing.Size(112, 22);
        this.pasteToolStripMenuItem1.Text = "Paste";
        // 
        // chkUrdu
        // 
        this.chkUrdu.AutoSize = true;
        this.chkUrdu.Checked = true;
        this.chkUrdu.CheckState = System.Windows.Forms.CheckState.Checked;
        this.chkUrdu.Location = new System.Drawing.Point(362, 64);
        this.chkUrdu.Name = "chkUrdu";
        this.chkUrdu.Size = new System.Drawing.Size(49, 17);
        this.chkUrdu.TabIndex = 4;
        this.chkUrdu.Text = "Urdu";
        this.chkUrdu.UseVisualStyleBackColor = true;
        // 
        // menuStrip1
        // 
        this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.windowToolStripMenuItem,
            this.helpToolStripMenuItem1});
        this.menuStrip1.Location = new System.Drawing.Point(0, 0);
        this.menuStrip1.Name = "menuStrip1";
        this.menuStrip1.Size = new System.Drawing.Size(740, 24);
        this.menuStrip1.TabIndex = 6;
        this.menuStrip1.Text = "menuStrip1";
        // 
        // fileToolStripMenuItem
        // 
        this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.toolStripSeparator,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator1,
            this.printToolStripMenuItem,
            this.printPreviewToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
        this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
        this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
        this.fileToolStripMenuItem.Text = "&File";
        // 
        // newToolStripMenuItem
        // 
        this.newToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripMenuItem.Image")));
        this.newToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
        this.newToolStripMenuItem.Name = "newToolStripMenuItem";
        this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
        this.newToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
        this.newToolStripMenuItem.Text = "&New";
        // 
        // openToolStripMenuItem
        // 
        this.openToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripMenuItem.Image")));
        this.openToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
        this.openToolStripMenuItem.Name = "openToolStripMenuItem";
        this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
        this.openToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
        this.openToolStripMenuItem.Text = "&Open";
        // 
        // toolStripSeparator
        // 
        this.toolStripSeparator.Name = "toolStripSeparator";
        this.toolStripSeparator.Size = new System.Drawing.Size(189, 6);
        // 
        // saveToolStripMenuItem
        // 
        this.saveToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripMenuItem.Image")));
        this.saveToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
        this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
        this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
        this.saveToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
        this.saveToolStripMenuItem.Text = "&Save";
        // 
        // saveAsToolStripMenuItem
        // 
        this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
        this.saveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                    | System.Windows.Forms.Keys.S)));
        this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
        this.saveAsToolStripMenuItem.Text = "Save &As";
        // 
        // toolStripSeparator1
        // 
        this.toolStripSeparator1.Name = "toolStripSeparator1";
        this.toolStripSeparator1.Size = new System.Drawing.Size(189, 6);
        // 
        // printToolStripMenuItem
        // 
        this.printToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("printToolStripMenuItem.Image")));
        this.printToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
        this.printToolStripMenuItem.Name = "printToolStripMenuItem";
        this.printToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
        this.printToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
        this.printToolStripMenuItem.Text = "&Print";
        this.printToolStripMenuItem.Click += new System.EventHandler(this.printToolStripMenuItem_Click);
        // 
        // printPreviewToolStripMenuItem
        // 
        this.printPreviewToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("printPreviewToolStripMenuItem.Image")));
        this.printPreviewToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
        this.printPreviewToolStripMenuItem.Name = "printPreviewToolStripMenuItem";
        this.printPreviewToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
        this.printPreviewToolStripMenuItem.Text = "Print Pre&view";
        this.printPreviewToolStripMenuItem.Click += new System.EventHandler(this.printPreviewToolStripMenuItem_Click);
        // 
        // toolStripSeparator2
        // 
        this.toolStripSeparator2.Name = "toolStripSeparator2";
        this.toolStripSeparator2.Size = new System.Drawing.Size(189, 6);
        // 
        // exitToolStripMenuItem
        // 
        this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
        this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
        this.exitToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
        this.exitToolStripMenuItem.Text = "E&xit";
        // 
        // editToolStripMenuItem
        // 
        this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem1,
            this.toolStripSeparator3,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.toolStripSeparator4,
            this.selectAllToolStripMenuItem1});
        this.editToolStripMenuItem.Name = "editToolStripMenuItem";
        this.editToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
        this.editToolStripMenuItem.Text = "&Edit";
        // 
        // undoToolStripMenuItem
        // 
        this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
        this.undoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
        this.undoToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
        this.undoToolStripMenuItem.Text = "&Undo";
        this.undoToolStripMenuItem.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
        // 
        // redoToolStripMenuItem1
        // 
        this.redoToolStripMenuItem1.Name = "redoToolStripMenuItem1";
        this.redoToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
        this.redoToolStripMenuItem1.Size = new System.Drawing.Size(167, 22);
        this.redoToolStripMenuItem1.Text = "&Redo";
        this.redoToolStripMenuItem1.Click += new System.EventHandler(this.redoToolStripMenuItem1_Click);
        // 
        // toolStripSeparator3
        // 
        this.toolStripSeparator3.Name = "toolStripSeparator3";
        this.toolStripSeparator3.Size = new System.Drawing.Size(164, 6);
        // 
        // cutToolStripMenuItem
        // 
        this.cutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cutToolStripMenuItem.Image")));
        this.cutToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
        this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
        this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
        this.cutToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
        this.cutToolStripMenuItem.Text = "Cu&t";
        this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
        // 
        // copyToolStripMenuItem
        // 
        this.copyToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("copyToolStripMenuItem.Image")));
        this.copyToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
        this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
        this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
        this.copyToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
        this.copyToolStripMenuItem.Text = "&Copy";
        this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
        // 
        // pasteToolStripMenuItem
        // 
        this.pasteToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("pasteToolStripMenuItem.Image")));
        this.pasteToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
        this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
        this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
        this.pasteToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
        this.pasteToolStripMenuItem.Text = "&Paste";
        this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
        // 
        // toolStripSeparator4
        // 
        this.toolStripSeparator4.Name = "toolStripSeparator4";
        this.toolStripSeparator4.Size = new System.Drawing.Size(164, 6);
        // 
        // selectAllToolStripMenuItem1
        // 
        this.selectAllToolStripMenuItem1.Name = "selectAllToolStripMenuItem1";
        this.selectAllToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
        this.selectAllToolStripMenuItem1.Size = new System.Drawing.Size(167, 22);
        this.selectAllToolStripMenuItem1.Text = "Select &All";
        this.selectAllToolStripMenuItem1.Click += new System.EventHandler(this.selectAllToolStripMenuItem1_Click);
        // 
        // toolsToolStripMenuItem
        // 
        this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customizeToolStripMenuItem,
            this.optionsToolStripMenuItem});
        this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
        this.toolsToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
        this.toolsToolStripMenuItem.Text = "&Tools";
        // 
        // customizeToolStripMenuItem
        // 
        this.customizeToolStripMenuItem.Name = "customizeToolStripMenuItem";
        this.customizeToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
        this.customizeToolStripMenuItem.Text = "&Customize";
        // 
        // optionsToolStripMenuItem
        // 
        this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
        this.optionsToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
        this.optionsToolStripMenuItem.Text = "&Options";
        // 
        // windowToolStripMenuItem
        // 
        this.windowToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Background_color,
            this.texBoxColorToolStripMenuItem});
        this.windowToolStripMenuItem.Name = "windowToolStripMenuItem";
        this.windowToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
        this.windowToolStripMenuItem.Text = "Window";
        this.windowToolStripMenuItem.Click += new System.EventHandler(this.windowToolStripMenuItem_Click);
        // 
        // Background_color
        // 
        this.Background_color.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AliceBlue,
            this.Cornflower,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3});
        this.Background_color.Name = "Background_color";
        this.Background_color.Size = new System.Drawing.Size(169, 22);
        this.Background_color.Text = "Background Color";
        this.Background_color.Click += new System.EventHandler(this.backgroundColorToolStripMenuItem_Click);
        // 
        // AliceBlue
        // 
        this.AliceBlue.Name = "AliceBlue";
        this.AliceBlue.Size = new System.Drawing.Size(176, 22);
        this.AliceBlue.Text = "Alice Blue (Default)";
        this.AliceBlue.Click += new System.EventHandler(this.greyToolStripMenuItem_Click);
        // 
        // Cornflower
        // 
        this.Cornflower.Name = "Cornflower";
        this.Cornflower.Size = new System.Drawing.Size(176, 22);
        this.Cornflower.Text = "CornflowerBlue";
        this.Cornflower.Click += new System.EventHandler(this.color3ToolStripMenuItem_Click);
        // 
        // toolStripMenuItem2
        // 
        this.toolStripMenuItem2.Name = "toolStripMenuItem2";
        this.toolStripMenuItem2.Size = new System.Drawing.Size(176, 22);
        this.toolStripMenuItem2.Text = "Indigo";
        this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
        // 
        // toolStripMenuItem3
        // 
        this.toolStripMenuItem3.Name = "toolStripMenuItem3";
        this.toolStripMenuItem3.Size = new System.Drawing.Size(176, 22);
        this.toolStripMenuItem3.Text = "5";
        this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
        // 
        // texBoxColorToolStripMenuItem
        // 
        this.texBoxColorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.color1ToolStripMenuItem,
            this.color2ToolStripMenuItem,
            this.color3ToolStripMenuItem,
            this.color4ToolStripMenuItem,
            this.color5ToolStripMenuItem});
        this.texBoxColorToolStripMenuItem.Name = "texBoxColorToolStripMenuItem";
        this.texBoxColorToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
        this.texBoxColorToolStripMenuItem.Text = "TexBox Color";
        this.texBoxColorToolStripMenuItem.Click += new System.EventHandler(this.texBoxColorToolStripMenuItem_Click);
        // 
        // color1ToolStripMenuItem
        // 
        this.color1ToolStripMenuItem.Name = "color1ToolStripMenuItem";
        this.color1ToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
        this.color1ToolStripMenuItem.Text = "DarkViolet";
        this.color1ToolStripMenuItem.Click += new System.EventHandler(this.color1ToolStripMenuItem_Click);
        // 
        // color2ToolStripMenuItem
        // 
        this.color2ToolStripMenuItem.Name = "color2ToolStripMenuItem";
        this.color2ToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
        this.color2ToolStripMenuItem.Text = "Color2";
        this.color2ToolStripMenuItem.Click += new System.EventHandler(this.color2ToolStripMenuItem_Click);
        // 
        // color3ToolStripMenuItem
        // 
        this.color3ToolStripMenuItem.Name = "color3ToolStripMenuItem";
        this.color3ToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
        this.color3ToolStripMenuItem.Text = "Color3";
        this.color3ToolStripMenuItem.Click += new System.EventHandler(this.color3ToolStripMenuItem_Click_1);
        // 
        // color4ToolStripMenuItem
        // 
        this.color4ToolStripMenuItem.Name = "color4ToolStripMenuItem";
        this.color4ToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
        this.color4ToolStripMenuItem.Text = "Color4";
        this.color4ToolStripMenuItem.Click += new System.EventHandler(this.color4ToolStripMenuItem_Click);
        // 
        // color5ToolStripMenuItem
        // 
        this.color5ToolStripMenuItem.Name = "color5ToolStripMenuItem";
        this.color5ToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
        this.color5ToolStripMenuItem.Text = "Color5";
        this.color5ToolStripMenuItem.Click += new System.EventHandler(this.color5ToolStripMenuItem_Click);
        // 
        // helpToolStripMenuItem1
        // 
        this.helpToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contentsToolStripMenuItem,
            this.indexToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.toolStripSeparator5,
            this.aboutToolStripMenuItem});
        this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
        this.helpToolStripMenuItem1.Size = new System.Drawing.Size(40, 20);
        this.helpToolStripMenuItem1.Text = "&Help";
        // 
        // contentsToolStripMenuItem
        // 
        this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
        this.contentsToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
        this.contentsToolStripMenuItem.Text = "&Contents";
        // 
        // indexToolStripMenuItem
        // 
        this.indexToolStripMenuItem.Name = "indexToolStripMenuItem";
        this.indexToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
        this.indexToolStripMenuItem.Text = "&Index";
        // 
        // searchToolStripMenuItem
        // 
        this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
        this.searchToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
        this.searchToolStripMenuItem.Text = "&Search";
        // 
        // toolStripSeparator5
        // 
        this.toolStripSeparator5.Name = "toolStripSeparator5";
        this.toolStripSeparator5.Size = new System.Drawing.Size(126, 6);
        // 
        // aboutToolStripMenuItem
        // 
        this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
        this.aboutToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
        this.aboutToolStripMenuItem.Text = "&About...";
        // 
        // ScrnNew
        // 
        this.ScrnNew.BackColor = System.Drawing.SystemColors.Menu;
        this.ScrnNew.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ScrnNew.BackgroundImage")));
        this.ScrnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
        this.ScrnNew.Location = new System.Drawing.Point(8, 55);
        this.ScrnNew.Name = "ScrnNew";
        this.ScrnNew.Size = new System.Drawing.Size(60, 53);
        this.ScrnNew.TabIndex = 7;
        this.ScrnNew.UseVisualStyleBackColor = false;
        this.ScrnNew.Click += new System.EventHandler(this.ScrnNew_Click);
        // 
        // ScrnOpen
        // 
        this.ScrnOpen.BackColor = System.Drawing.SystemColors.Menu;
        this.ScrnOpen.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ScrnOpen.BackgroundImage")));
        this.ScrnOpen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
        this.ScrnOpen.Location = new System.Drawing.Point(79, 55);
        this.ScrnOpen.Name = "ScrnOpen";
        this.ScrnOpen.Size = new System.Drawing.Size(60, 53);
        this.ScrnOpen.TabIndex = 8;
        this.ScrnOpen.UseVisualStyleBackColor = false;
        this.ScrnOpen.Click += new System.EventHandler(this.ScrnOpen_Click);
        // 
        // button3
        // 
        this.button3.BackColor = System.Drawing.SystemColors.Menu;
        this.button3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button3.BackgroundImage")));
        this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
        this.button3.Location = new System.Drawing.Point(150, 55);
        this.button3.Name = "button3";
        this.button3.Size = new System.Drawing.Size(60, 53);
        this.button3.TabIndex = 9;
        this.button3.UseVisualStyleBackColor = false;
        this.button3.Click += new System.EventHandler(this.button3_Click);
        // 
        // button4
        // 
        this.button4.BackColor = System.Drawing.SystemColors.Menu;
        this.button4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button4.BackgroundImage")));
        this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
        this.button4.Location = new System.Drawing.Point(221, 55);
        this.button4.Name = "button4";
        this.button4.Size = new System.Drawing.Size(60, 53);
        this.button4.TabIndex = 10;
        this.button4.UseVisualStyleBackColor = false;
        this.button4.Click += new System.EventHandler(this.button4_Click);
        // 
        // button5
        // 
        this.button5.BackColor = System.Drawing.SystemColors.Menu;
        this.button5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button5.BackgroundImage")));
        this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
        this.button5.Location = new System.Drawing.Point(292, 55);
        this.button5.Name = "button5";
        this.button5.Size = new System.Drawing.Size(60, 53);
        this.button5.TabIndex = 11;
        this.button5.UseVisualStyleBackColor = false;
        // 
        // button7
        // 
        this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.button7.Location = new System.Drawing.Point(447, 58);
        this.button7.Name = "button7";
        this.button7.Size = new System.Drawing.Size(29, 23);
        this.button7.TabIndex = 13;
        this.button7.Text = "U";
        this.button7.UseVisualStyleBackColor = true;
        this.button7.Click += new System.EventHandler(this.button7_Click);
        // 
        // button6
        // 
        this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.button6.Location = new System.Drawing.Point(412, 58);
        this.button6.Name = "button6";
        this.button6.Size = new System.Drawing.Size(29, 23);
        this.button6.TabIndex = 12;
        this.button6.Text = "B";
        this.button6.UseVisualStyleBackColor = true;
        this.button6.Click += new System.EventHandler(this.button6_Click);
        // 
        // button1
        // 
        this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.button1.Location = new System.Drawing.Point(482, 56);
        this.button1.Name = "button1";
        this.button1.Size = new System.Drawing.Size(29, 26);
        this.button1.TabIndex = 13;
        this.button1.Text = "I";
        this.button1.UseVisualStyleBackColor = true;
        this.button1.Click += new System.EventHandler(this.button7_Click);
        // 
        // statusStrip1
        // 
        this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel3});
        this.statusStrip1.Location = new System.Drawing.Point(0, 417);
        this.statusStrip1.Name = "statusStrip1";
        this.statusStrip1.Size = new System.Drawing.Size(740, 22);
        this.statusStrip1.TabIndex = 15;
        this.statusStrip1.Text = "statusStrip1";
        // 
        // toolStripStatusLabel1
        // 
        this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
        this.toolStripStatusLabel1.Size = new System.Drawing.Size(59, 17);
        this.toolStripStatusLabel1.Text = "CharCount";
        // 
        // toolStripStatusLabel2
        // 
        this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
        this.toolStripStatusLabel2.Size = new System.Drawing.Size(633, 17);
        this.toolStripStatusLabel2.Spring = true;
        // 
        // toolStripStatusLabel3
        // 
        this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
        this.toolStripStatusLabel3.Size = new System.Drawing.Size(33, 17);
        this.toolStripStatusLabel3.Text = "Zoom";
        // 
        // toolStrip1
        // 
        this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printToolStripButton,
            this.toolStripSeparator6,
            this.cutToolStripButton,
            this.toolStripButton1,
            this.toolStripSeparator7,
            this.toolStripSeparator9,
            this.toolStripSeparator11,
            this.toolStripSeparator12,
            this.toolStripSeparator13,
            this.AlignL,
            this.AlignC,
            this.AlignR,
            this.toolStripSeparator10,
            this.toolStripSeparator8,
            this.ZoomIn,
            this.ZoomOut,
            this.toolStripComboBox1,
            this.toolStripComboBox2});
        this.toolStrip1.Location = new System.Drawing.Point(0, 24);
        this.toolStrip1.Name = "toolStrip1";
        this.toolStrip1.Size = new System.Drawing.Size(740, 25);
        this.toolStrip1.TabIndex = 16;
        this.toolStrip1.Text = "toolStrip1";
        // 
        // printToolStripButton
        // 
        this.printToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        this.printToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("printToolStripButton.Image")));
        this.printToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
        this.printToolStripButton.Name = "printToolStripButton";
        this.printToolStripButton.Size = new System.Drawing.Size(23, 22);
        this.printToolStripButton.Text = "&Print";
        // 
        // toolStripSeparator6
        // 
        this.toolStripSeparator6.Name = "toolStripSeparator6";
        this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
        // 
        // cutToolStripButton
        // 
        this.cutToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        this.cutToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("cutToolStripButton.Image")));
        this.cutToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
        this.cutToolStripButton.Name = "cutToolStripButton";
        this.cutToolStripButton.Size = new System.Drawing.Size(23, 22);
        this.cutToolStripButton.Text = "C&ut";
        // 
        // toolStripButton1
        // 
        this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
        this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
        this.toolStripButton1.Name = "toolStripButton1";
        this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
        this.toolStripButton1.Text = "&Paste";
        // 
        // toolStripSeparator7
        // 
        this.toolStripSeparator7.Name = "toolStripSeparator7";
        this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
        // 
        // toolStripSeparator9
        // 
        this.toolStripSeparator9.Name = "toolStripSeparator9";
        this.toolStripSeparator9.Size = new System.Drawing.Size(6, 25);
        // 
        // toolStripSeparator11
        // 
        this.toolStripSeparator11.Name = "toolStripSeparator11";
        this.toolStripSeparator11.Size = new System.Drawing.Size(6, 25);
        // 
        // toolStripSeparator12
        // 
        this.toolStripSeparator12.Name = "toolStripSeparator12";
        this.toolStripSeparator12.Size = new System.Drawing.Size(6, 25);
        // 
        // toolStripSeparator13
        // 
        this.toolStripSeparator13.Name = "toolStripSeparator13";
        this.toolStripSeparator13.Size = new System.Drawing.Size(6, 25);
        // 
        // AlignL
        // 
        this.AlignL.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
        this.AlignL.Image = ((System.Drawing.Image)(resources.GetObject("AlignL.Image")));
        this.AlignL.ImageTransparentColor = System.Drawing.Color.Magenta;
        this.AlignL.Name = "AlignL";
        this.AlignL.Size = new System.Drawing.Size(23, 22);
        this.AlignL.Text = "L";
        // 
        // AlignC
        // 
        this.AlignC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
        this.AlignC.Image = ((System.Drawing.Image)(resources.GetObject("AlignC.Image")));
        this.AlignC.ImageTransparentColor = System.Drawing.Color.Magenta;
        this.AlignC.Name = "AlignC";
        this.AlignC.Size = new System.Drawing.Size(23, 22);
        this.AlignC.Text = "C";
        // 
        // AlignR
        // 
        this.AlignR.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
        this.AlignR.Image = ((System.Drawing.Image)(resources.GetObject("AlignR.Image")));
        this.AlignR.ImageTransparentColor = System.Drawing.Color.Magenta;
        this.AlignR.Name = "AlignR";
        this.AlignR.Size = new System.Drawing.Size(23, 22);
        this.AlignR.Text = "R";
        // 
        // toolStripSeparator10
        // 
        this.toolStripSeparator10.Name = "toolStripSeparator10";
        this.toolStripSeparator10.Size = new System.Drawing.Size(6, 25);
        // 
        // toolStripSeparator8
        // 
        this.toolStripSeparator8.Name = "toolStripSeparator8";
        this.toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
        // 
        // ZoomIn
        // 
        this.ZoomIn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
        this.ZoomIn.Image = ((System.Drawing.Image)(resources.GetObject("ZoomIn.Image")));
        this.ZoomIn.ImageTransparentColor = System.Drawing.Color.Magenta;
        this.ZoomIn.Name = "ZoomIn";
        this.ZoomIn.Size = new System.Drawing.Size(23, 22);
        this.ZoomIn.Text = "+";
        this.ZoomIn.Click += new System.EventHandler(this.toolStripButton11_Click);
        // 
        // ZoomOut
        // 
        this.ZoomOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
        this.ZoomOut.Image = ((System.Drawing.Image)(resources.GetObject("ZoomOut.Image")));
        this.ZoomOut.ImageTransparentColor = System.Drawing.Color.Magenta;
        this.ZoomOut.Name = "ZoomOut";
        this.ZoomOut.Size = new System.Drawing.Size(23, 22);
        this.ZoomOut.Text = "-";
        // 
        // toolStripComboBox1
        // 
        this.toolStripComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.toolStripComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
        this.toolStripComboBox1.Name = "toolStripComboBox1";
        this.toolStripComboBox1.Size = new System.Drawing.Size(121, 25);
        // 
        // toolStripComboBox2
        // 
        this.toolStripComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.toolStripComboBox2.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
        this.toolStripComboBox2.Name = "toolStripComboBox2";
        this.toolStripComboBox2.Size = new System.Drawing.Size(121, 25);
        // 
        // timer
        // 
        this.timer.Enabled = true;
        this.timer.Interval = 1;
        // 
        // openWork
        // 
        this.openWork.Filter = "Text Files|*.txt|RTF files|*.rtf";
        this.openWork.Title = "Open Work";
        // 
        // saveWork
        // 
        this.saveWork.FileName = "Text Files|*.txt|RTF files|*.rtf|";
        this.saveWork.Title = "Save Work";
        // 
        // Form1
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
        this.ClientSize = new System.Drawing.Size(740, 439);
        this.Controls.Add(this.toolStrip1);
        this.Controls.Add(this.statusStrip1);
        this.Controls.Add(this.button1);
        this.Controls.Add(this.button7);
        this.Controls.Add(this.button6);
        this.Controls.Add(this.button5);
        this.Controls.Add(this.button4);
        this.Controls.Add(this.button3);
        this.Controls.Add(this.ScrnOpen);
        this.Controls.Add(this.ScrnNew);
        this.Controls.Add(this.menuStrip1);
        this.Controls.Add(this.chkUrdu);
        this.Controls.Add(this.textBox1);
        this.MinimumSize = new System.Drawing.Size(748, 473);
        this.Name = "Form1";
        this.Text = "Form1";
        this.Load += new System.EventHandler(this.Form1_Load);
        this.rcMenu.ResumeLayout(false);
        this.menuStrip1.ResumeLayout(false);
        this.menuStrip1.PerformLayout();
        this.statusStrip1.ResumeLayout(false);
        this.statusStrip1.PerformLayout();
        this.toolStrip1.ResumeLayout(false);
        this.toolStrip1.PerformLayout();
        this.ResumeLayout(false);
        this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox textBox1;
    private System.Windows.Forms.CheckBox chkUrdu;
    private System.Windows.Forms.MenuStrip menuStrip1;
    private System.Windows.Forms.ToolStripMenuItem windowToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem Background_color;
    private System.Windows.Forms.ToolStripMenuItem AliceBlue;
    private System.Windows.Forms.ToolStripMenuItem Cornflower;
    private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
    private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
    private System.Windows.Forms.ToolStripMenuItem texBoxColorToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem color1ToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem color2ToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem color3ToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem color4ToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem color5ToolStripMenuItem;
    private System.Windows.Forms.Button ScrnNew;
    private System.Windows.Forms.Button ScrnOpen;
    private System.Windows.Forms.Button button3;
    private System.Windows.Forms.Button button4;
    private System.Windows.Forms.Button button5;
    private System.Windows.Forms.Button button7;
    private System.Windows.Forms.Button button6;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
    private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem printPreviewToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem1;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
    private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
    private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem1;
    private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem customizeToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
    private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem indexToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
    private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    private System.Windows.Forms.StatusStrip statusStrip1;


    private System.Windows.Forms.ToolStrip toolStrip1;
    private System.Windows.Forms.ToolStripButton printToolStripButton;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
    private System.Windows.Forms.ToolStripButton cutToolStripButton;
    private System.Windows.Forms.ToolStripButton toolStripButton1;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
    private System.Windows.Forms.ToolStripButton AlignL;
    private System.Windows.Forms.ToolStripButton AlignC;
    private System.Windows.Forms.ToolStripButton AlignR;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
    private System.Windows.Forms.ToolStripButton ZoomIn;
    private System.Windows.Forms.ToolStripButton ZoomOut;
    private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
    private System.Windows.Forms.ToolStripComboBox toolStripComboBox2;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
    private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
    private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
    private System.Windows.Forms.ContextMenuStrip rcMenu;
    private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem1;
    private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
    private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem1;
    private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem1;
    private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem1;
    private System.Windows.Forms.Timer timer;
    private System.Windows.Forms.OpenFileDialog openWork;
    private System.Windows.Forms.SaveFileDialog saveWork;



  }
}

