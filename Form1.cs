using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace WindowsApplication7
{
  public partial class Form1 : Form
  {
    bool changeposition = false;
    public Form1()
    {
      InitializeComponent();
    }

    private void button1_Click(object sender, EventArgs e)
    {
    }
      // start
    private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (chkUrdu.Checked == false)
      {
        e.Handled = false;
      }
      else if (!changeposition)
      {
        if (Convert.ToInt32(e.KeyChar) == 8) return;
        if (Convert.ToInt32(e.KeyChar) == 13) return; 
        //{
        //  int SelectionStart=textBox1.SelectionStart;
        //  StringBuilder SB = new StringBuilder();
        //  SB.AppendLine();
        //  string part1 = textBox1.Text.Substring(0,SelectionStart);
        //  string part2 = textBox1.Text.Substring(SelectionStart, textBox1.Text.Length - SelectionStart);
        //  textBox1.Text = part2 + SB.ToString() + part1;
        //  textBox1.SelectionStart = part2.Length + part1.Length;
        //  e.Handled = true;
        //  return;
        //}
        
        char chr = e.KeyChar;
        char GetUrduChar = Urdu.space;
        char prev = Urdu.space;
        char Next = Urdu.space;
        bool UrduChr = false;
        if (textBox1.Text.Length >= 1)
        {
          if (textBox1.Text.Substring(textBox1.SelectionStart, textBox1.Text.Length - textBox1.SelectionStart).Length >= 1)
          {
            prev = Convert.ToChar(textBox1.Text.Substring(textBox1.SelectionStart, 1));
          }
          if (textBox1.SelectionStart > 0)
          {
            Next = Convert.ToChar(textBox1.Text.Substring(textBox1.SelectionStart - 1, 1));
          }
        }
        GetUrduChar = Urdu.GetUrduLetter(e.KeyChar, prev, Next);
        e.KeyChar = GetUrduChar == '~' ? e.KeyChar : GetUrduChar;
        UrduChr = (GetUrduChar == '~' || GetUrduChar == ' ') ? false : true;
        changeposition = !(chr == e.KeyChar);

        if (Urdu.prevJoiningCharacter == true)
        {
          char temp = prev;

          if (UrduChr == false)
          {
            temp = Urdu.getPrevNonJoiningLetter(prev);
          }
          else
          {
            temp = Urdu.getPrevJoiningLetter(prev);
          }
          if (temp != prev)
          {
            int SelectionStart = textBox1.SelectionStart;
            textBox1.Text = textBox1.Text.Remove(SelectionStart, 1);
            textBox1.Text = textBox1.Text.Insert(SelectionStart, temp.ToString());
            textBox1.SelectionStart=SelectionStart;
          }
        }

        if (Urdu.nextJoiningCharacter == true)
        {
          if (textBox1.SelectionStart > 0)
          {
            char temp = Next;

            if (UrduChr == false)
            {
              temp = Urdu.getNextNonJoiningLetter(Next);
            }
            else
            {
              temp = Urdu.getNextJoiningLetter(Next);
            }
            if (Next != temp)
            {
              int SelectionStart = textBox1.SelectionStart;
              textBox1.Text = textBox1.Text.Remove(SelectionStart - 1, 1);
              textBox1.Text = textBox1.Text.Insert(SelectionStart-1, temp.ToString());
              textBox1.SelectionStart = SelectionStart;
            }
          }
        }



        if (UrduChr == false)
        {
          if (Convert.ToInt32(e.KeyChar) == 32)
          {
            changeposition = true;
          }
        }
      }
      else
      {
        e.Handled = true;
      }
    }
      // stops
      
      // starts 2
    private void textBox1_KeyUp(object sender, KeyEventArgs e)
    {
      if (changeposition)
      {
        if (textBox1.SelectionStart > 0)
        {
          textBox1.Select(textBox1.SelectionStart - 1, 0);
          changeposition = false;
        }
      }
    }
      // stop2
    private void Form1_Load(object sender, EventArgs e)
    {
        this.BackColor = System.Drawing.Color.AliceBlue;
    }

    private void button1_Click_1(object sender, EventArgs e)
    {

    }
      //start3
    private void textBox1_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Delete )
      {
        int IncrementNext = 0;
        int IncrementPrev = 0;
        int Current = 0;
        if (e.KeyCode == Keys.Delete)
        {
          IncrementPrev = 1;
          IncrementNext = -1;
          Current = 0;
        }
        
        char prev = Urdu.space;
        char Next = Urdu.space;
        if (textBox1.Text.Length >= 1)
        {
          if (textBox1.SelectionStart < textBox1.Text.Length && textBox1.SelectionStart + Current >0)
          if (Urdu.IsCurrnetCharacterJoinedBothSides(Convert.ToChar(textBox1.Text.Substring(textBox1.SelectionStart + Current, 1)))) return;

          if (textBox1.Text.Substring(textBox1.SelectionStart, textBox1.Text.Length - textBox1.SelectionStart).Length >= 1)
          {
            if (Convert.ToInt32(textBox1.SelectionStart + IncrementPrev) < textBox1.Text.Length)
            {
              //prev = Convert.ToChar(textBox1.Text.Substring(textBox1.SelectionStart + 1, 1));
              prev = Convert.ToChar(textBox1.Text.Substring(textBox1.SelectionStart + IncrementPrev, 1));
            }
          }
          if (textBox1.SelectionStart > 0)
          {
            //Next = Convert.ToChar(textBox1.Text.Substring(textBox1.SelectionStart - 1, 1));
            Next = Convert.ToChar(textBox1.Text.Substring(textBox1.SelectionStart +  IncrementNext, 1));
          }

          if (prev != Urdu.space)
          {
            char temp = prev;
            temp = Urdu.getPrevNonJoiningLetter(prev);
            if (temp != prev)
            {
              //textBox1.Text = textBox1.Text.Remove(textBox1.SelectionStart + 1, 1);
              //textBox1.Text = textBox1.Text.Insert(textBox1.SelectionStart + 1, temp.ToString());
              textBox1.Text = textBox1.Text.Remove(textBox1.SelectionStart +IncrementPrev, 1);
              textBox1.Text = textBox1.Text.Insert(textBox1.SelectionStart + IncrementPrev, temp.ToString());
            }
          }

          if (Next != Urdu.space)
          {
            char temp = Next;
            temp = Urdu.getNextNonJoiningLetter(Next);
            if (Next != temp)
            {
              textBox1.Text = textBox1.Text.Remove(textBox1.SelectionStart + IncrementNext, 1);
              textBox1.Text = textBox1.Text.Insert(textBox1.SelectionStart, temp.ToString());
              textBox1.SelectionStart = textBox1.SelectionStart + 1;
            }
          }
        }
      }
      else if (e.KeyCode == Keys.Back)
      {
      
        char prev = Urdu.space;
        char Next = Urdu.space;
        if (textBox1.Text.Length >= 1)
        {
          if (textBox1.SelectionStart < textBox1.Text.Length && textBox1.SelectionStart > 0)
            if (Urdu.IsCurrnetCharacterJoinedBothSides(Convert.ToChar(textBox1.Text.Substring(textBox1.SelectionStart-1 , 1)))) return;

          if (textBox1.Text.Substring(textBox1.SelectionStart, textBox1.Text.Length - textBox1.SelectionStart).Length >= 1)
          {
            if (Convert.ToInt32(textBox1.SelectionStart ) < textBox1.Text.Length)
            {
              prev = Convert.ToChar(textBox1.Text.Substring(textBox1.SelectionStart, 1));
            }
          }
          //if (textBox1.SelectionStart > 0)
          //{
          //  //Next = Convert.ToChar(textBox1.Text.Substring(textBox1.SelectionStart - 1, 1));
          //  Next = Convert.ToChar(textBox1.Text.Substring(textBox1.SelectionStart + IncrementNext, 1));
          //}

          if (prev != Urdu.space)
          {
            char temp = prev;
            temp = Urdu.getPrevNonJoiningLetter(prev);
            if (temp != prev)
            {
              int SelectionStart = textBox1.SelectionStart;
              textBox1.Text = textBox1.Text.Remove(textBox1.SelectionStart , 1);
              textBox1.Text = textBox1.Text.Insert(SelectionStart, temp.ToString());
              textBox1.SelectionStart = SelectionStart;
            }
          }

          //if (Next != Urdu.space)
          //{
          //  char temp = Next;
          //  temp = Urdu.getNextNonJoiningLetter(Next);
          //  if (Next != temp)
          //  {
          //    textBox1.Text = textBox1.Text.Remove(textBox1.SelectionStart + IncrementNext, 1);
          //    textBox1.Text = textBox1.Text.Insert(textBox1.SelectionStart, temp.ToString());
          //    textBox1.SelectionStart = textBox1.SelectionStart + 1;
          //  }
          //}
        }
      }
    }
    //stop3
    private void textBox1_TextChanged(object sender, EventArgs e)
    {
        textBox1.BackColor = System.Drawing.Color.White;
            
    }

    private void fileMenu_Click(object sender, EventArgs e)
    {

    }

    private void backgroundColorToolStripMenuItem_Click(object sender, EventArgs e)
    {

    }

    private void greyToolStripMenuItem_Click(object sender, EventArgs e)
    {
        this.BackColor = System.Drawing.Color.AliceBlue;
    }

    private void color3ToolStripMenuItem_Click(object sender, EventArgs e)
    {
        this.BackColor = System.Drawing.Color.CornflowerBlue;
    }

    private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
    {
        textBox1.SelectAll();
    }

    private void copyMenu_Click(object sender, EventArgs e)
    {
        if (textBox1.SelectionLength > 0)
            textBox1.Copy();
    }

    private void cutMenu_Click(object sender, EventArgs e)
    {
        if (textBox1.SelectedText != "")
            textBox1.Cut();
    }

    private void pasteMenu_Click(object sender, EventArgs e)
    {
        if (Clipboard.GetDataObject().GetDataPresent(DataFormats.Text) == true)
            textBox1.Paste();
    }

    private void undoMenu_Click(object sender, EventArgs e)
    {
        if (textBox1.CanUndo == true)
            textBox1.Undo();
        textBox1.ClearUndo();
    }

    private void redoToolStripMenuItem_Click(object sender, EventArgs e)
    {
        
    }

    private void toolStripMenuItem2_Click(object sender, EventArgs e)
    {
        this.BackColor = System.Drawing.Color.Indigo;
       
    }

    private void toolStripMenuItem3_Click(object sender, EventArgs e)
    {
        this.BackColor = System.Drawing.Color.LightSalmon;
    }

    private void texBoxColorToolStripMenuItem_Click(object sender, EventArgs e)
    {

    }

    private void color1ToolStripMenuItem_Click(object sender, EventArgs e)
    {
        textBox1.BackColor = System.Drawing.Color.DarkViolet;
    }

    private void color2ToolStripMenuItem_Click(object sender, EventArgs e)
    {
        textBox1.BackColor = System.Drawing.Color.LightPink;
    }

    private void color3ToolStripMenuItem_Click_1(object sender, EventArgs e)
    {
        textBox1.BackColor = System.Drawing.Color.LightSeaGreen;
    }

    private void color4ToolStripMenuItem_Click(object sender, EventArgs e)
    {
        textBox1.BackColor = System.Drawing.Color.Lime;
    }

    private void color5ToolStripMenuItem_Click(object sender, EventArgs e)
    {
        textBox1.BackColor = System.Drawing.Color.MediumSeaGreen;
    }

    private void button6_Click(object sender, EventArgs e)
    {
        
        if (textBox1.SelectedText.Length == 0)
            return;
        if (textBox1.Font.Bold )
        {
            textBox1.Font = new Font(textBox1.Font, FontStyle.Regular);
            
        }
        else
        {
            if (textBox1.SelectionLength > 0)
            {
                textBox1.Font = new Font(textBox1.Font, FontStyle.Bold);
            }
            else {
                return;
            }
        }
        
        
        
        }

    private void button7_Click(object sender, EventArgs e)
    {
        
    }

    private void ScrnNew_Click(object sender, EventArgs e)
    {

        textBox1.Clear();
    }

    private void quitMenu_Click(object sender, EventArgs e)
    {

    }

    private void ScrnOpen_Click(object sender, EventArgs e)
    {
        
        
        //Shows the openFileDialog
        OpenFileDialog openFileDialog1 = new OpenFileDialog();
        openFileDialog1.ShowDialog();
        //Reads the text file
        System.IO.StreamReader OpenFile = new System.IO.StreamReader(openFileDialog1.FileName);
        //Displays the text file in the textBox
        textBox1.Text = OpenFile.ReadToEnd();
        //Closes the proccess
        OpenFile.Close();
        

    }

    private void button3_Click(object sender, EventArgs e)
    {
        //Determines the text file to save to
        SaveFileDialog saveFileDialog1 = new SaveFileDialog();
        System.IO.StreamWriter SaveFile = new System.IO.StreamWriter(saveFileDialog1.FileName);
        //Writes the text to the file
        SaveFile.WriteLine(textBox1.Text);
        //Closes the proccess
        SaveFile.Close();


    }

    private void button4_Click(object sender, EventArgs e)
    {
        //Open the saveFileDialog
        SaveFileDialog saveFileDialog1 = new SaveFileDialog();
        saveFileDialog1.ShowDialog();
        //Determines the text file to save to
        System.IO.StreamWriter SaveFile = new System.IO.StreamWriter(saveFileDialog1.FileName);
        //Writes the text to the file
        SaveFile.WriteLine(textBox1.Text);
        //Closes the proccess
        SaveFile.Close();


    }

    private void windowToolStripMenuItem_Click(object sender, EventArgs e)
    {

    }

    private void printToolStripMenuItem_Click(object sender, EventArgs e)
    {
        
    }
    private void printPreviewToolStripMenuItem_Click(object sender, EventArgs e)
    {
        /*
        //Declare preview as a new PrintPreviewDialog
        System.Drawing.Printing.PrintDocument prntDoc = new System.Drawing.Printing.PrintDocument();
        PrintPreviewDialog preview = new PrintPreviewDialog();
        //Declare prntDoc_PrintPage as a new EventHandler for prntDoc's Print Page

        prntDoc.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(prntDoc_PrintPage);
        //Set the PrintPreview's Document equal to prntDoc
        preview.Document = prntDoc;
        //Show the PrintPreview Dialog
        if (preview.ShowDialog(this) == DialogResult.OK)
        {
        //Generate the PrintPreview
        prntDoc.Print();
        }
        */

    }

    private void undoToolStripMenuItem_Click(object sender, EventArgs e)
    {
        textBox1.Undo();
    }

    private void redoToolStripMenuItem1_Click(object sender, EventArgs e)
    {
        textBox1.Undo();
    }

    private void cutToolStripMenuItem_Click(object sender, EventArgs e)
    {
        textBox1.Cut();
    }

    private void copyToolStripMenuItem_Click(object sender, EventArgs e)
    {
        textBox1.Copy();
    }

    private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
    {
        textBox1.Paste();
    }

    private void selectAllToolStripMenuItem1_Click(object sender, EventArgs e)
    {
        textBox1.SelectAll();
    }

    private void toolStripButton11_Click(object sender, EventArgs e)
    {
        //if (textBox1.Resize == 63)
        //{
         //   return;
        //}else{
          //  textBox1. = textBox1.Resize + 1;
        //}
    }

  }
}



