using System;
using System.Collections.Generic;
using System.Text;

namespace WindowsApplication7
{
  public class Urdu
  {
    public static bool prevJoiningCharacter = false;
    public static bool nextJoiningCharacter = false;
    public static bool currentJoiningCharacter = false;

    public static char space = '\u0020';

    public static char alif = '\uFE8D';
    public static char alif_Joined_AtEnd = '\uFE8E';
    public static char alif_With_Madd = '\uFE81';
    
    public static char be = '\uFE8F';
    public static char be_Joined_AtEnd = '\uFE90';
    public static char be_Joined_AtStart = '\uFE91';
    public static char be_Joined_AtMidle = '\uFE92';

    public static char pe = '\uFB56';
    public static char pe_Joined_AtEnd = '\uFB57';
    public static char pe_Joined_AtStart = '\uFb58';
    public static char pe_Joined_AtMidle = '\uFB59';

    
    public static char te = '\uFE95';
    public static char te_Joined_AtEnd = '\uFE96';
    public static char te_Joined_AtStart = '\uFE97';
    public static char te_Joined_AtMidle = '\uFE98';

    public static char ta = '\uFB66';
    public static char ta_Joined_AtEnd = '\uFB67';
    public static char ta_Joined_AtStart = '\uFB68';
    public static char ta_Joined_AtMidle = '\uFB69';

    public static char se = '\uFE99';
    public static char se_Joined_AtEnd = '\uFE9A';
    public static char se_Joined_AtStart = '\uFE9B';
    public static char se_Joined_AtMidle = '\uFE9C';

    public static char jeem = '\uFE9D';
    public static char jeem_Joined_AtEnd = '\uFE9E';
    public static char jeem_Joined_AtStart = '\uFE9F';
    public static char jeem_Joined_AtMidle = '\uFEA0';

    public static char che = '\uFB7A';
    public static char che_Joined_AtEnd = '\uFB7B';
    public static char che_Joined_AtStart = '\uFB7C';
    public static char che_Joined_AtMidle = '\uFB7D';

    public static char he = '\uFEA1';
    public static char he_Joined_AtEnd = '\uFEA2';
    public static char he_Joined_AtStart = '\uFEA3';
    public static char he_Joined_AtMidle = '\uFEA4';

    public static char hedoaankhwali = '\uFBAA';
    public static char hedoaankhwali_Joined_AtEnd = '\uFBAB';
    public static char hedoaankhwali_Joined_AtStart = '\uFBAC';
    public static char hedoaankhwali_Joined_AtMidle = '\uFBAD';
    
    public static char heekaankhwali = '\uFBA6';
    public static char heekaankhwali_Joined_AtEnd = '\uFBA7';
    public static char heekaankhwali_Joined_AtStart = '\uFBA8';
    public static char heekaankhwali_Joined_AtMidle = '\uFBA9';

    public static char khe = '\uFEA5';
    public static char khe_Joined_AtEnd = '\uFEA6';
    public static char khe_Joined_AtStart = '\uFEA7';
    public static char khe_Joined_AtMidle = '\uFEA8';

    public static char daal = '\uFEA9';
    public static char daal_Joined_AtEnd = '\uFEA9';

    public static char dhaal = '\u0688';
    public static char dhaal_Joined_AtEnd = '\u0688';

    public static char zaal = '\uFEAB';
    public static char zaal_Joined_AtEnd = '\uFEAC';

    public static char re = '\uFEAD';
    public static char re_Joined_AtEnd = '\uFEAE';

    public static char rhe = '\uFB8C';
    public static char rhe_Joined_AtEnd = '\uFB8D';

    public static char ze = '\uFEAF';
    public static char ze_Joined_AtEnd = '\uFEB0';

    public static char seen = '\uFEB1';
    public static char seen_Joined_AtStart = '\uFEB3';
    public static char seen_Joined_AtEnd = '\uFEB2';
    public static char seen_Joined_AtMidle = '\uFEB4';

    public static char sheen = '\uFEB5';
    public static char sheen_Joined_AtEnd = '\uFEB6';
    public static char sheen_Joined_AtStart = '\uFEB7';
    public static char sheen_Joined_AtMidle = '\uFEB8';

    public static char swad = '\uFEB9';
    public static char swad_Joined_AtEnd = '\uFEBA';
    public static char swad_Joined_AtStart = '\uFEBB';
    public static char swad_Joined_AtMidle = '\uFEBC';

    public static char dwad = '\uFEBD';
    public static char dwad_Joined_AtEnd = '\uFEBE';
    public static char dwad_Joined_AtStart = '\uFEC0';
    public static char dwad_Joined_AtMidle = '\uFEBF';

    public static char toe = '\uFEC1';
    public static char toe_Joined_AtEnd = '\uFEC2';
    public static char toe_Joined_AtStart = '\uFEC3';
    public static char toe_Joined_AtMidle = '\uFEC4';

    public static char zoe = '\uFEC5';
    public static char zoe_Joined_AtEnd = '\uFEC6';
    public static char zoe_Joined_AtStart = '\uFEC7';
    public static char zoe_Joined_AtMidle = '\uFEC8';

    public static char an = '\uFEC9';
    public static char an_Joined_AtEnd = '\uFECA';
    public static char an_Joined_AtStart = '\uFECB';
    public static char an_Joined_AtMidle = '\uFECC';

    public static char ghain = '\uFECD';
    public static char ghain_Joined_AtEnd = '\uFECE';
    public static char ghain_Joined_AtStart = '\uFECF';
    public static char ghain_Joined_AtMidle = '\uFED0';

    public static char fe = '\uFED1';
    public static char fe_Joined_AtEnd = '\uFED2';
    public static char fe_Joined_AtStart = '\uFED3';
    public static char fe_Joined_AtMidle = '\uFED4';

    public static char kaaf = '\uFED5';
    public static char kaaf_Joined_AtEnd = '\uFED6';
    public static char kaaf_Joined_AtStart = '\uFED7';
    public static char kaaf_Joined_AtMidle = '\uFED8';

    public static char kiaf = '\uFED9';
    public static char kiaf_Joined_AtEnd = '\uFEDA';
    public static char kiaf_Joined_AtStart = '\uFEDB';
    public static char kiaf_Joined_AtMidle = '\uFEDC';

    public static char gaaf = '\u06AF';
    public static char gaaf_Joined_AtEnd = '\uFB93';
    public static char gaaf_Joined_AtStart = '\uFB94';
    public static char gaaf_Joined_AtMidle = '\uFB95';

    public static char laam = '\uFEDD';
    public static char laam_Joined_AtEnd = '\uFEDE';
    public static char laam_Joined_AtStart = '\uFEDF';
    public static char laam_Joined_AtMidle = '\uFEE0';

    public static char meem = '\uFEE1';
    public static char meem_Joined_AtEnd = '\uFEE2';
    public static char meem_Joined_AtStart = '\uFEE3';
    public static char meem_Joined_AtMidle = '\uFEE4';

    public static char noon = '\uFEE5';
    public static char noon_Joined_AtEnd = '\uFEE6';
    public static char noon_Joined_AtStart = '\uFEE7';
    public static char noon_Joined_AtMidle = '\uFEE8';

    public static char noonGunna = '\uFB9F';


    public static char wao = '\uFEED';
    public static char wao_Joined_AtEnd = '\uFEEE';

    public static char hamza = '\uFE80';
    public static char hamza_Joined_AtEnd = '\uFE8A';
    public static char hamza_Joined_AtStart = '\uFE8B';
    public static char hamza_Joined_AtMidle = '\uFE8C';


    public static char chotiye = '\uFBFC';
    public static char chotiye_Joined_AtEnd = '\uFBFD';
    public static char chotiye_Joined_AtStart = '\uFBFE';
    public static char chotiye_Joined_AtMidle = '\uFBFF';
    
    public static char bariye = '\uFBAE';
    public static char bariye_Joined_AtEnd = '\uFBAF';


    public static char zero = '\u0660';
    public static char one = '\u0661';
    public static char two = '\u0662';
    public static char three = '\u0663';
    public static char four = '\u0664';
    public static char five = '\u0665';
    public static char six = '\u0666';
    public static char seven = '\u0667';
    public static char eight = '\u0668';
    public static char nine = '\u0669';
    public static char questionmark = '\u061F';
    public static char desh = '\u0640';


    public static char GetUrduLetter(char c,char prev,char next)
    {
      prevJoiningCharacter=IsPrevJoiningCharacter(prev);
      nextJoiningCharacter=IsNextJoiningCharacter(next);

      switch (c.ToString())
      {
        case "":
          return space;
        case "a":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          { 
          return alif_Joined_AtEnd;
          }
            return alif;
        case "b":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return be_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return be_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return be_Joined_AtMidle;
          }
          return be;
        case "c":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return che_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return che_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return che_Joined_AtMidle;
          }
          return che;
          
        case "d":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return daal_Joined_AtEnd;
          }
          
          return daal;
          
        case "e":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return an_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return an_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return an_Joined_AtMidle;
          }
          return an;
          
        case "f":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return fe_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return fe_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return fe_Joined_AtMidle;
          }
          return fe;
          
        case "g":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return gaaf_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return gaaf_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return gaaf_Joined_AtMidle;
          }
          return gaaf;
          
        case "h":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return hedoaankhwali_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return hedoaankhwali_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return hedoaankhwali_Joined_AtMidle;
          }
          return hedoaankhwali;
          
        case "i":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return chotiye_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return chotiye_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return chotiye_Joined_AtMidle;
          }
          return chotiye;
          
        case "j":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return jeem_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return jeem_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return jeem_Joined_AtMidle;
          }
          return jeem;
          
        case "k":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return kiaf_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return kiaf_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return kiaf_Joined_AtMidle;
          }
          return kiaf;
          
        case "l":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return laam_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return laam_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return laam_Joined_AtMidle;
          }
          return laam;
          
        case "m":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return meem_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return meem_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return meem_Joined_AtMidle;
          }
          return meem;
          
        case "n":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return noon_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return noon_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return noon_Joined_AtMidle;
          }
          return noon;
          
        case "o":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return heekaankhwali_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return heekaankhwali_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return heekaankhwali_Joined_AtMidle;
          }
          return heekaankhwali; 
          
        case "p":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return pe_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return pe_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return pe_Joined_AtMidle;
          }
          return pe;
          
        case "q":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return kaaf_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return kaaf_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return kaaf_Joined_AtMidle;
          }
          return kaaf;
          
        case "r":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return re_Joined_AtEnd;
          }
          return re;
          
        case "s":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return seen_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return seen_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return seen_Joined_AtMidle;
          }
          return seen;
          
        case "t":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return te_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return te_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return te_Joined_AtMidle;
          }
          return te;
          
        case "u":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return hamza_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return hamza_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return hamza_Joined_AtMidle;
          }
          return hamza;
          
        case "v":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return toe_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return toe_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return toe_Joined_AtMidle;
          }
          return toe;
          
        case "w":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return wao_Joined_AtEnd;
          }
          return wao;
          
        case "x":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return sheen_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return sheen_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return sheen_Joined_AtMidle;
          }
          return sheen;
          
        case "y":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return bariye_Joined_AtEnd;
          }
          return bariye;

        case "z":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return ze_Joined_AtEnd;
          }
          return ze;
        case "A":
          return alif_With_Madd;
        case "B":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return be_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return be_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return be_Joined_AtMidle;
          }
          return be;
        case "C":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return se_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return se_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return se_Joined_AtMidle;
          }
          return se;

        case "D":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return dhaal_Joined_AtEnd;
          }
          return dhaal;

        case "E":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return an_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return an_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return an_Joined_AtMidle;
          }
          return an;

        case "F":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return fe_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return fe_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return fe_Joined_AtMidle;
          }
          return fe;

        case "G":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return ghain_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return ghain_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return ghain_Joined_AtMidle;
          }
          return ghain;

        case "H":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return he_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return he_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return he_Joined_AtMidle;
          }
          return he;

        case "I":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return chotiye_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return chotiye_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return chotiye_Joined_AtMidle;
          }
          return chotiye;

        case "J":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return dwad_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return dwad_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return dwad_Joined_AtMidle;
          }
          return dwad;

        case "K":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return khe_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return khe_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return khe_Joined_AtMidle;
          }
          return khe;

        case "L":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return laam_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return laam_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return laam_Joined_AtMidle;
          }
          return laam;

        case "M":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return meem_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return meem_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return meem_Joined_AtMidle;
          }
          return meem;

        case "N":
          //if (prevJoiningCharacter && !nextJoiningCharacter)
          //{
          //  return noonGunna;
          //}
          //else if (!prevJoiningCharacter && nextJoiningCharacter)
          //{
          //  return noon_Joined_AtStart;
          //}
          //else if (prevJoiningCharacter && nextJoiningCharacter)
          //{
          //  return noon_Joined_AtMidle;
          //}
          return noonGunna;

        case "O":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return heekaankhwali_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return heekaankhwali_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return heekaankhwali_Joined_AtMidle;
          }
          return heekaankhwali;

        case "P":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return pe_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return pe_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return pe_Joined_AtMidle;
          }
          return pe;

        case "Q":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return kaaf_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return kaaf_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return kaaf_Joined_AtMidle;
          }
          return kaaf;

        case "R":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return rhe_Joined_AtEnd;
          }
          return rhe;

        case "S":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return swad_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return swad_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return swad_Joined_AtMidle;
          }
          return swad;

        case "T":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return ta_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return ta_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return ta_Joined_AtMidle;
          }
          return ta;

        case "U":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return hamza_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return hamza_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return hamza_Joined_AtMidle;
          }
          return hamza;
        case "V":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return zoe_Joined_AtEnd;
          }
          else if (!prevJoiningCharacter && nextJoiningCharacter)
          {
            return zoe_Joined_AtStart;
          }
          else if (prevJoiningCharacter && nextJoiningCharacter)
          {
            return zoe_Joined_AtMidle;
          }
          return zoe;

        case "W":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return wao_Joined_AtEnd;
          }
          return wao;

        case "X":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return ze_Joined_AtEnd;
          }
          return ze;

        case "Y":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return bariye_Joined_AtEnd;
          }
          return bariye;

        case "Z":
          if (prevJoiningCharacter && !nextJoiningCharacter)
          {
            return zaal_Joined_AtEnd;
          }
          return zaal;
        case "0":
          return zero;
        case "1":
          return one;
        case "2":
          return two;
        case "3":
          return three;
        case "4":
          return four;
        case "5":
          return five;
        case "6":
          return six;
        case "7":
          return seven;
        case "8":
          return eight;
        case "9":
          return nine;
        case "-":
          return desh;
        default:
          if (Convert.ToInt32(c) == 32)
          return space;
          break;
      }
      return '~';
    }

    public static bool IsNextJoiningCharacter(char Next)
    {
      if (Next == alif || Next == alif_Joined_AtEnd)
      {
        return true;
      }
      else if (Next == be || Next == be_Joined_AtEnd || Next == be_Joined_AtMidle || Next == be_Joined_AtStart)
      {
        return true;
      }
      else if (Next == pe || Next == pe_Joined_AtEnd || Next == pe_Joined_AtMidle || Next == pe_Joined_AtStart)
      {
        return true;
      }
      else if (Next == te || Next == te_Joined_AtEnd || Next == te_Joined_AtMidle || Next == te_Joined_AtStart)
      {
        return true;
      }
      else if (Next == ta || Next == ta_Joined_AtEnd || Next == ta_Joined_AtMidle || Next == ta_Joined_AtStart)
      {
        return true;
      }
      else if (Next == se || Next == se_Joined_AtEnd || Next == se_Joined_AtMidle || Next == se_Joined_AtStart)
      {
        return true;
      }
      else if (Next == jeem || Next == jeem_Joined_AtEnd || Next == jeem_Joined_AtMidle || Next == jeem_Joined_AtStart)
      {
        return true;
      }
      else if (Next == che || Next == che_Joined_AtEnd || Next == che_Joined_AtMidle || Next == che_Joined_AtStart)
      {
        return true;
      }
      else if (Next == he || Next == he_Joined_AtEnd || Next == he_Joined_AtMidle || Next == he_Joined_AtStart)
      {
        return true;
      }
      else if (Next == khe || Next == khe_Joined_AtEnd || Next == khe_Joined_AtMidle || Next == khe_Joined_AtStart)
      {
        return true;
      }
      else if (Next == daal|| Next == daal_Joined_AtEnd)
      {
        return true;
      }
      else if (Next == zaal || Next == zaal_Joined_AtEnd)
      {
        return true;
      }
      else if (Next == re|| Next == re_Joined_AtEnd)
      {
        return true;
      }
      else if (Next == rhe || Next == rhe_Joined_AtEnd)
      {
        return true;
      }
      else if (Next == ze || Next == ze_Joined_AtEnd)
      {
        return true;
      }
      else if (Next == wao || Next == wao_Joined_AtEnd)
      {
        return true;
      }
      else if (Next == seen || Next == seen_Joined_AtEnd || Next == seen_Joined_AtMidle || Next == seen_Joined_AtStart)
      {
        return true;
      }
      else if (Next == sheen || Next == sheen_Joined_AtEnd || Next == sheen_Joined_AtMidle || Next == sheen_Joined_AtStart)
      {
        return true;
      }
      else if (Next == swad || Next == swad_Joined_AtEnd || Next == swad_Joined_AtMidle || Next == swad_Joined_AtStart)
      {
        return true;
      }
      else if (Next == dwad || Next == dwad_Joined_AtEnd || Next == dwad_Joined_AtMidle || Next == dwad_Joined_AtStart)
      {
        return true;
      }
      else if (Next == toe || Next == toe_Joined_AtEnd || Next == toe_Joined_AtMidle || Next == toe_Joined_AtStart)
      {
        return true;
      }
      else if (Next == zoe || Next == zoe_Joined_AtEnd || Next == zoe_Joined_AtMidle || Next == zoe_Joined_AtStart)
      {
        return true;
      }
      else if (Next == an || Next == an_Joined_AtEnd || Next == an_Joined_AtMidle || Next == an_Joined_AtStart)
      {
        return true;
      }
      else if (Next == ghain || Next == ghain_Joined_AtEnd || Next == ghain_Joined_AtMidle || Next == ghain_Joined_AtStart)
      {
        return true;
      }
      else if (Next == fe || Next == fe_Joined_AtEnd || Next == fe_Joined_AtMidle || Next == fe_Joined_AtStart)
      {
        return true;
      }
      else if (Next == kaaf || Next == kaaf_Joined_AtEnd || Next == kaaf_Joined_AtMidle || Next == kaaf_Joined_AtStart)
      {
        return true;
      }
      else if (Next == kiaf || Next == kiaf_Joined_AtEnd || Next == kiaf_Joined_AtMidle || Next == kiaf_Joined_AtStart)
      {
        return true;
      }
      else if (Next == gaaf || Next== gaaf_Joined_AtMidle || Next== gaaf_Joined_AtStart || Next== gaaf_Joined_AtEnd)
      {
        return true;
      }
      else if (Next == laam || Next == laam_Joined_AtEnd || Next == laam_Joined_AtMidle || Next == laam_Joined_AtStart)
      {
        return true;
      }
      else if (Next == meem || Next == meem_Joined_AtEnd || Next == meem_Joined_AtMidle || Next == meem_Joined_AtStart)
      {
        return true;
      }
      else if (Next == noon || Next == noon_Joined_AtEnd || Next == noon_Joined_AtMidle || Next == noon_Joined_AtStart)
      {
        return true;
      }
      else if (Next == heekaankhwali || Next == heekaankhwali_Joined_AtEnd || Next == heekaankhwali_Joined_AtMidle || Next == heekaankhwali_Joined_AtStart)
      {
        return true;
      }
      else if (Next == hedoaankhwali || Next == hedoaankhwali_Joined_AtEnd || Next == hedoaankhwali_Joined_AtMidle || Next == hedoaankhwali_Joined_AtStart)
      {
        return true;
      }
      else if (Next == hamza || Next == hamza_Joined_AtEnd || Next == hamza_Joined_AtMidle || Next == hamza_Joined_AtStart || Next == hamza_Joined_AtEnd)
      {
        return true;
      }

      return false;
    }
    public static bool IsPrevJoiningCharacter(char prev)
    {
      if (prev == be || prev == be_Joined_AtEnd || prev == be_Joined_AtMidle || prev == be_Joined_AtStart)
      {
        return true;
      }
      else if (prev == pe || prev == pe_Joined_AtEnd || prev == pe_Joined_AtMidle || prev == pe_Joined_AtStart)
      {
        return true;
      }
      else if (prev == te || prev == te_Joined_AtEnd || prev == te_Joined_AtMidle || prev == te_Joined_AtStart)
      {
        return true;
      }
      else if (prev == ta || prev == ta_Joined_AtEnd || prev == ta_Joined_AtMidle || prev == ta_Joined_AtStart)
      {
        return true;
      }
      else if (prev == se || prev == se_Joined_AtEnd || prev == se_Joined_AtMidle || prev == se_Joined_AtStart)
      {
        return true;
      }
      else if (prev == jeem || prev == jeem_Joined_AtEnd || prev == jeem_Joined_AtMidle || prev == jeem_Joined_AtStart)
      {
        return true;
      }
      else if (prev == che || prev == che_Joined_AtEnd || prev == che_Joined_AtMidle || prev == che_Joined_AtStart)
      {
        return true;
      }
      else if (prev == he || prev == he_Joined_AtEnd || prev == he_Joined_AtMidle || prev == he_Joined_AtStart)
      {
        return true;
      }
      else if (prev == khe || prev == khe_Joined_AtEnd || prev == khe_Joined_AtMidle || prev == khe_Joined_AtStart)
      {
        return true;
      }
      else if (prev == seen || prev == seen_Joined_AtEnd || prev == seen_Joined_AtMidle || prev == seen_Joined_AtStart)
      {
        return true;
      }
      else if (prev == sheen || prev == sheen_Joined_AtEnd || prev == sheen_Joined_AtMidle || prev == sheen_Joined_AtStart)
      {
        return true;
      }
      else if (prev == swad || prev == swad_Joined_AtEnd || prev == swad_Joined_AtMidle || prev == swad_Joined_AtStart)
      {
        return true;
      }
      else if (prev == dwad || prev == dwad_Joined_AtEnd || prev == dwad_Joined_AtMidle || prev == dwad_Joined_AtStart)
      {
        return true;
      }
      else if (prev == toe || prev == toe_Joined_AtEnd || prev == toe_Joined_AtMidle || prev == toe_Joined_AtStart)
      {
        return true;
      }
      else if (prev == zoe || prev == zoe_Joined_AtEnd || prev == zoe_Joined_AtMidle || prev == zoe_Joined_AtStart)
      {
        return true;
      }
      else if (prev == an || prev == an_Joined_AtEnd || prev == an_Joined_AtMidle || prev == an_Joined_AtStart)
      {
        return true;
      }
      else if (prev == ghain || prev == ghain_Joined_AtEnd || prev == ghain_Joined_AtMidle || prev == ghain_Joined_AtStart)
      {
        return true;
      }
      else if (prev == fe || prev == fe_Joined_AtEnd || prev == fe_Joined_AtMidle || prev == fe_Joined_AtStart)
      {
        return true;
      }
      else if (prev == kaaf || prev == kaaf_Joined_AtEnd || prev == kaaf_Joined_AtMidle || prev == kaaf_Joined_AtStart)
      {
        return true;
      }
      else if (prev == kiaf || prev == kiaf_Joined_AtEnd || prev == kiaf_Joined_AtMidle || prev == kiaf_Joined_AtStart)
      {
        return true;
      }
      else if (prev == gaaf || prev == gaaf_Joined_AtMidle || prev == gaaf_Joined_AtStart || prev == gaaf_Joined_AtEnd)
      {
        return true;
      }
      else if (prev == laam || prev == laam_Joined_AtEnd || prev == laam_Joined_AtMidle || prev == laam_Joined_AtStart)
      {
        return true;
      }
      else if (prev == meem || prev == meem_Joined_AtEnd || prev == meem_Joined_AtMidle || prev == meem_Joined_AtStart)
      {
        return true;
      }
      else if (prev == noon || prev == noon_Joined_AtEnd || prev == noon_Joined_AtMidle || prev == noon_Joined_AtStart)
      {
        return true;
      }
      else if (prev == heekaankhwali || prev == heekaankhwali_Joined_AtEnd || prev == heekaankhwali_Joined_AtMidle || prev == heekaankhwali_Joined_AtStart)
      {
        return true;
      }
      else if (prev == hedoaankhwali || prev == hedoaankhwali_Joined_AtEnd || prev == hedoaankhwali_Joined_AtMidle || prev == hedoaankhwali_Joined_AtStart)
      {
        return true;
      }
      else if (prev == chotiye || prev == chotiye_Joined_AtEnd || prev == chotiye_Joined_AtMidle || prev == chotiye_Joined_AtStart)
      {
        return true;
      }
      else if (prev == hamza || prev == hamza_Joined_AtMidle || prev == hamza_Joined_AtStart || prev == hamza_Joined_AtEnd)
      {
        return true;
      }
      return false;
    }
    public static bool IsCurrnetCharacterJoinedBothSides(char chr)
    {
      if (chr == be_Joined_AtMidle )
      {
        return true;
      }
      else if (chr == pe_Joined_AtMidle )
      {
        return true;
      }
      else if (chr == te_Joined_AtMidle )
      {
        return true;
      }
      else if (chr == ta_Joined_AtMidle )
      {
        return true;
      }
      else if (chr == se_Joined_AtMidle )
      {
        return true;
      }
      else if (chr == jeem_Joined_AtMidle )
      {
        return true;
      }
      else if (chr == che_Joined_AtMidle )
      {
        return true;
      }
      else if ( chr == he_Joined_AtMidle  )
      {
        return true;
      }
      else if ( chr == khe_Joined_AtMidle  )
      {
        return true;
      }
      else if ( chr == seen_Joined_AtMidle  )
      {
        return true;
      }
      else if ( chr == sheen_Joined_AtMidle  )
      {
        return true;
      }
      else if ( chr == swad_Joined_AtMidle )
      {
        return true;
      }
      else if (  chr == dwad_Joined_AtMidle  )
      {
        return true;
      }
      else if (  chr == toe_Joined_AtMidle  )
      {
        return true;
      }
      else if (  chr == zoe_Joined_AtMidle  )
      {
        return true;
      }
      else if (  chr == an_Joined_AtMidle  )
      {
        return true;
      }
      else if ( chr == ghain_Joined_AtMidle )
      {
        return true;
      }
      else if ( chr == fe_Joined_AtMidle )
      {
        return true;
      }
      else if ( chr == kaaf_Joined_AtMidle  )
      {
        return true;
      }
      else if ( chr == kiaf_Joined_AtMidle )
      {
        return true;
      }
      else if (  chr == gaaf_Joined_AtStart  )
      {
        return true;
      }
      else if ( chr == laam_Joined_AtMidle  )
      {
        return true;
      }
      else if ( chr == meem_Joined_AtMidle )
      {
        return true;
      }
      else if ( chr == noon_Joined_AtMidle )
      {
        return true;
      }
      else if (  chr == heekaankhwali_Joined_AtMidle)
      {
        return true;
      }
      else if (chr == hedoaankhwali_Joined_AtMidle )
      {
        return true;
      }
      else if (chr == chotiye_Joined_AtMidle )
      {
        return true;
      }
      else if (chr == hamza_Joined_AtMidle )
      {
        return true;
      }
      return false;
    }
    public static char getPrevJoiningLetter(char chr)
    {

    if (chr==be)return be_Joined_AtStart;
    if (chr==be_Joined_AtEnd) return be_Joined_AtMidle; 

      if (chr==pe)return pe_Joined_AtStart;
    if (chr==pe_Joined_AtEnd ) return pe_Joined_AtMidle;
    
    if (chr==te )return te_Joined_AtStart;
    if (chr==te_Joined_AtEnd)return te_Joined_AtMidle;
    
    if (chr==ta) return ta_Joined_AtStart;
    if (chr==ta_Joined_AtEnd )return ta_Joined_AtMidle;
    
    if (chr==se) return se_Joined_AtStart;
    if (chr==se_Joined_AtEnd )return se_Joined_AtMidle;

    if (chr==jeem)return jeem_Joined_AtStart;
    if (chr==jeem_Joined_AtEnd) return jeem_Joined_AtMidle;

      if (chr==che)return che_Joined_AtStart;
    if (chr==che_Joined_AtEnd )return che_Joined_AtMidle;
    

    if (chr==he)return he_Joined_AtStart;
    if (chr==he_Joined_AtEnd )return he_Joined_AtMidle;
    

    if (chr==hedoaankhwali)return hedoaankhwali_Joined_AtStart;
    if (chr == hedoaankhwali_Joined_AtEnd) return hedoaankhwali_Joined_AtMidle;
    
    if (chr==heekaankhwali)return heekaankhwali_Joined_AtStart;
    if (chr==heekaankhwali_Joined_AtEnd)return heekaankhwali_Joined_AtMidle;

    if (chr==khe)return khe_Joined_AtStart;
    if (chr==khe_Joined_AtEnd )return khe_Joined_AtMidle;

    if (chr==seen )return seen_Joined_AtStart;
    if (chr==seen_Joined_AtEnd )return seen_Joined_AtMidle;

      if (chr==sheen )return sheen_Joined_AtStart;
    if (chr==sheen_Joined_AtEnd )return sheen_Joined_AtMidle;
    
    if (chr==swad )return swad_Joined_AtStart;
    if (chr==swad_Joined_AtEnd )return swad_Joined_AtMidle;

    if (chr==dwad )return dwad_Joined_AtStart;
    if (chr==dwad_Joined_AtEnd )return dwad_Joined_AtMidle;
    

    if (chr==toe )return toe_Joined_AtStart;
    if (chr==toe_Joined_AtEnd )return toe_Joined_AtMidle;
    

    if (chr==zoe )return zoe_Joined_AtStart;
    if (chr==zoe_Joined_AtEnd )return zoe_Joined_AtMidle;
    

    if (chr==an )return an_Joined_AtStart;
    if (chr==an_Joined_AtEnd )return an_Joined_AtMidle;
    

    if (chr==ghain )return ghain_Joined_AtStart;
    if (chr==ghain_Joined_AtEnd )return ghain_Joined_AtMidle;
    

    if (chr==fe )return fe_Joined_AtStart;
    if (chr==fe_Joined_AtEnd)return fe_Joined_AtMidle;
    

    if (chr==kaaf )return kaaf_Joined_AtStart;
    if (chr==kaaf_Joined_AtEnd)return kaaf_Joined_AtMidle;
    

    if (chr==kiaf )return kiaf_Joined_AtStart;
    if (chr==kiaf_Joined_AtEnd )return kiaf_Joined_AtMidle;

    if (chr == gaaf) return gaaf_Joined_AtStart;
    if (chr == gaaf_Joined_AtEnd) return gaaf_Joined_AtMidle;

    if (chr==laam )return laam_Joined_AtStart;
    if (chr==laam_Joined_AtEnd )return laam_Joined_AtMidle;
    

    if (chr==meem )return meem_Joined_AtStart;
    if (chr==meem_Joined_AtEnd )return meem_Joined_AtMidle;
    

    if (chr==noon )return noon_Joined_AtStart;
    if (chr==noon_Joined_AtEnd ) return noon_Joined_AtMidle;

    if (chr == hamza) return hamza_Joined_AtStart;
    if (chr == hamza_Joined_AtEnd) return hamza_Joined_AtMidle;

    if (chr==chotiye )return chotiye_Joined_AtStart;
    if (chr==chotiye_Joined_AtEnd )return chotiye_Joined_AtMidle;
      return chr;
      
    }
    public static char getPrevNonJoiningLetter(char chr)
    {

      if (/*chr == be_Joined_AtEnd ||*/ chr == be_Joined_AtMidle) return be;
      if (/*chr == be_Joined_AtEnd ||*/ chr == be_Joined_AtStart) return be;

      if (/*chr==pe_Joined_AtEnd ||*/ chr == pe_Joined_AtMidle) return pe;
      if (/*chr==pe_Joined_AtEnd ||*/ chr == pe_Joined_AtStart) return pe;

      if (/*chr==te_Joined_AtEnd ||*/ chr == te_Joined_AtMidle) return te;
      if (/*chr==te_Joined_AtEnd ||*/ chr == te_Joined_AtStart) return te;

      if (/*chr==ta_Joined_AtEnd ||*/ chr == ta_Joined_AtMidle) return ta;
      if (/*chr==ta_Joined_AtEnd ||*/ chr == ta_Joined_AtStart) return ta;

      if (/*chr==se_Joined_AtEnd ||*/ chr == se_Joined_AtMidle) return se_Joined_AtStart ;
      if (/*chr==se_Joined_AtEnd ||*/ chr == se_Joined_AtStart) return se;

      if (/*chr==jeem_Joined_AtEnd ||*/ chr == jeem_Joined_AtMidle) return jeem;
      if (/*chr==jeem_Joined_AtEnd ||*/ chr == jeem_Joined_AtStart) return jeem;

      if (/*chr==che_Joined_AtEnd ||*/ chr == che_Joined_AtMidle) return che;
      if (/*chr==che_Joined_AtEnd ||*/ chr == che_Joined_AtStart) return che;

      if (/*chr==he_Joined_AtEnd ||*/ chr == he_Joined_AtMidle) return he;
      if (/*chr==he_Joined_AtEnd ||*/ chr == he_Joined_AtStart) return he;

      if (/*chr==hedoaankhwali_Joined_AtEnd ||*/ chr == hedoaankhwali_Joined_AtMidle) return hedoaankhwali;
      if (/*chr==hedoaankhwali_Joined_AtEnd ||*/ chr == hedoaankhwali_Joined_AtStart) return hedoaankhwali;

      if (/*chr==heekaankhwali_Joined_AtEnd ||*/ chr == heekaankhwali_Joined_AtMidle ) return heekaankhwali;
      if (/*chr==heekaankhwali_Joined_AtEnd ||*/ chr == heekaankhwali_Joined_AtStart) return heekaankhwali;

      if (/*chr==khe_Joined_AtEnd ||*/ chr == khe_Joined_AtMidle) return khe;
      if (/*chr==khe_Joined_AtEnd ||*/ chr == khe_Joined_AtStart) return khe;

      if (/*chr==seen_Joined_AtEnd ||*/ chr == seen_Joined_AtMidle) return seen;
      if (/*chr==seen_Joined_AtEnd ||*/ chr == seen_Joined_AtStart) return seen;

      if (/*chr==sheen_Joined_AtEnd ||*/ chr == sheen_Joined_AtMidle) return sheen;
      if (/*chr==sheen_Joined_AtEnd ||*/ chr == sheen_Joined_AtStart) return sheen;

      if (/*chr==swad_Joined_AtEnd ||*/ chr == swad_Joined_AtMidle) return swad;
      if (/*chr==swad_Joined_AtEnd ||*/ chr == swad_Joined_AtStart) return swad;

      if (/*chr==dwad_Joined_AtEnd ||*/ chr == dwad_Joined_AtMidle) return dwad;
      if (/*chr==dwad_Joined_AtEnd ||*/ chr == dwad_Joined_AtStart) return dwad;

      if (/*chr==toe_Joined_AtEnd ||*/ chr == toe_Joined_AtMidle) return toe;
      if (/*chr==toe_Joined_AtEnd ||*/ chr == toe_Joined_AtStart) return toe;

      if (/*chr==zoe_Joined_AtEnd ||*/ chr == zoe_Joined_AtMidle) return zoe;
      if (/*chr==zoe_Joined_AtEnd ||*/ chr == zoe_Joined_AtStart) return zoe;

      if (/*chr==an_Joined_AtEnd ||*/ chr == an_Joined_AtMidle) return an;
      if (/*chr==an_Joined_AtEnd ||*/ chr == an_Joined_AtStart) return an;

      if (/*chr==ghain_Joined_AtEnd ||*/ chr == ghain_Joined_AtMidle) return ghain;
      if (/*chr==ghain_Joined_AtEnd ||*/ chr == ghain_Joined_AtStart) return ghain;

      if (/*chr==fe_Joined_AtEnd ||*/ chr == fe_Joined_AtMidle) return fe;
      if (/*chr==fe_Joined_AtEnd ||*/ chr == fe_Joined_AtStart) return fe;

      if (/*chr==kaaf_Joined_AtEnd ||*/ chr == kaaf_Joined_AtMidle) return kaaf;
      if (/*chr==kaaf_Joined_AtEnd ||*/ chr == kaaf_Joined_AtStart) return kaaf;

      if (/*chr==kiaf_Joined_AtEnd ||*/ chr == kiaf_Joined_AtMidle ) return kiaf;
      if (/*chr==kiaf_Joined_AtEnd ||*/ chr == kiaf_Joined_AtStart) return kiaf;

      if (/*chr==gaaf_Joined_AtEnd ||*/ chr == gaaf_Joined_AtMidle) return gaaf;
      if (/*chr==gaaf_Joined_AtEnd ||*/ chr == gaaf_Joined_AtStart) return gaaf;
      
      if (/*chr==laam_Joined_AtEnd ||*/ chr == laam_Joined_AtMidle) return laam;
      if (/*chr==laam_Joined_AtEnd ||*/ chr == laam_Joined_AtStart) return laam;

      if (/*chr==meem_Joined_AtEnd ||*/ chr == meem_Joined_AtMidle) return meem;
      if (/*chr==meem_Joined_AtEnd ||*/ chr == meem_Joined_AtStart) return meem;

      if (/*chr==noon_Joined_AtEnd ||*/ chr == noon_Joined_AtMidle) return noon;
      if (/*chr==noon_Joined_AtEnd ||*/ chr == noon_Joined_AtStart) return noon;

      if (/*chr==chotiye_Joined_AtEnd ||*/ chr == hamza_Joined_AtMidle) return hamza;
      if (/*chr==chotiye_Joined_AtEnd ||*/ chr == hamza_Joined_AtStart) return hamza;

      if (/*chr==chotiye_Joined_AtEnd ||*/ chr == chotiye_Joined_AtMidle) return chotiye;
      if (/*chr==chotiye_Joined_AtEnd ||*/ chr == chotiye_Joined_AtStart) return chotiye;
      return chr;
    }

    public static char getNextJoiningLetter(char chr)
    {
      if (chr == alif) return alif_Joined_AtEnd;

      if (chr == be) return be_Joined_AtEnd;
      if (chr == be_Joined_AtEnd) return be_Joined_AtMidle;

      if (chr == pe) return pe_Joined_AtEnd;
      if (chr == pe_Joined_AtEnd) return pe_Joined_AtMidle;

      if (chr == te) return te_Joined_AtEnd;
      if (chr == te_Joined_AtEnd) return te_Joined_AtMidle;

      if (chr == ta) return ta_Joined_AtEnd;
      if (chr == ta_Joined_AtEnd) return ta_Joined_AtMidle;

      if (chr == se) return se_Joined_AtEnd;
      if (chr == se_Joined_AtEnd) return se_Joined_AtMidle;

      if (chr == jeem) return jeem_Joined_AtEnd;
      if (chr == jeem_Joined_AtEnd) return jeem_Joined_AtMidle;

      if (chr == che) return che_Joined_AtEnd;
      if (chr == che_Joined_AtEnd) return che_Joined_AtMidle;


      if (chr == he) return he_Joined_AtEnd;
      if (chr == he_Joined_AtEnd) return he_Joined_AtMidle;


      if (chr == hedoaankhwali) return hedoaankhwali_Joined_AtEnd;
      if (chr == hedoaankhwali_Joined_AtEnd) return hedoaankhwali_Joined_AtMidle;

      if (chr == heekaankhwali) return heekaankhwali_Joined_AtEnd;
      if (chr == heekaankhwali_Joined_AtEnd) return heekaankhwali_Joined_AtMidle;

      if (chr == khe) return khe_Joined_AtEnd;
      if (chr == khe_Joined_AtEnd) return khe_Joined_AtMidle;

      if (chr == daal) return daal_Joined_AtEnd;

      if (chr == dhaal) return dhaal_Joined_AtEnd;

      if (chr == re) return re_Joined_AtEnd;

      if (chr == rhe) return rhe_Joined_AtEnd;

      if (chr == ze) return ze_Joined_AtEnd;


      if (chr == seen) return seen_Joined_AtEnd;
      if (chr == seen_Joined_AtEnd) return seen_Joined_AtMidle;

      if (chr == sheen) return sheen_Joined_AtEnd;
      if (chr == sheen_Joined_AtEnd) return sheen_Joined_AtMidle;

      if (chr == swad) return swad_Joined_AtEnd;
      if (chr == swad_Joined_AtEnd) return swad_Joined_AtMidle;

      if (chr == dwad) return dwad_Joined_AtEnd;
      if (chr == dwad_Joined_AtEnd) return dwad_Joined_AtMidle;


      if (chr == toe) return toe_Joined_AtEnd;
      if (chr == toe_Joined_AtEnd) return toe_Joined_AtMidle;


      if (chr == zoe) return zoe_Joined_AtEnd;
      if (chr == zoe_Joined_AtEnd) return zoe_Joined_AtMidle;


      if (chr == an) return an_Joined_AtEnd;
      if (chr == an_Joined_AtEnd) return an_Joined_AtMidle;


      if (chr == ghain) return ghain_Joined_AtEnd;
      if (chr == ghain_Joined_AtEnd) return ghain_Joined_AtMidle;


      if (chr == fe) return fe_Joined_AtEnd;
      if (chr == fe_Joined_AtEnd) return fe_Joined_AtMidle;


      if (chr == kaaf) return kaaf_Joined_AtEnd;
      if (chr == kaaf_Joined_AtEnd) return kaaf_Joined_AtMidle;


      if (chr == kiaf) return kiaf_Joined_AtEnd;
      if (chr == kiaf_Joined_AtEnd) return kiaf_Joined_AtMidle;

      if (chr == gaaf) return gaaf_Joined_AtEnd;
      if (chr == gaaf_Joined_AtEnd) return gaaf_Joined_AtMidle;


      if (chr == laam) return laam_Joined_AtEnd;
      if (chr == laam_Joined_AtEnd) return laam_Joined_AtMidle;


      if (chr == meem) return meem_Joined_AtEnd;
      if (chr == meem_Joined_AtEnd) return meem_Joined_AtMidle;


      if (chr == noon) return noon_Joined_AtEnd;
      if (chr == noon_Joined_AtEnd) return noon_Joined_AtMidle;

      if (chr == wao) return wao_Joined_AtEnd;
      if (chr == bariye) return bariye_Joined_AtEnd;

      if (chr == hamza) return hamza_Joined_AtEnd;
      if (chr == hamza_Joined_AtStart) return hamza_Joined_AtEnd;

      if (chr == chotiye) return chotiye_Joined_AtEnd;
      if (chr == chotiye_Joined_AtEnd) return chotiye_Joined_AtMidle;
      return chr;

    }
    public static char getNextNonJoiningLetter(char chr)
    {

      if (/*chr == be_Joined_AtEnd ||*/ chr == be_Joined_AtMidle) return be_Joined_AtStart;

      if (/*chr==pe_Joined_AtEnd ||*/ chr == pe_Joined_AtMidle) return pe_Joined_AtStart;

      if (/*chr==te_Joined_AtEnd ||*/ chr == te_Joined_AtMidle) return te_Joined_AtStart;

      if (/*chr==ta_Joined_AtEnd ||*/ chr == ta_Joined_AtMidle) return ta_Joined_AtStart;

      if (/*chr==se_Joined_AtEnd ||*/ chr == se_Joined_AtMidle) return se_Joined_AtStart;

      if (/*chr==jeem_Joined_AtEnd ||*/ chr == jeem_Joined_AtMidle) return jeem_Joined_AtStart;

      if (/*chr==che_Joined_AtEnd ||*/ chr == che_Joined_AtMidle) return che_Joined_AtStart;

      if (/*chr==he_Joined_AtEnd ||*/ chr == he_Joined_AtMidle) return he_Joined_AtStart;

      if (/*chr==hedoaankhwali_Joined_AtEnd ||*/ chr == hedoaankhwali_Joined_AtMidle) return hedoaankhwali_Joined_AtStart;

      if (/*chr==heekaankhwali_Joined_AtEnd ||*/ chr == heekaankhwali_Joined_AtMidle) return heekaankhwali_Joined_AtStart;

      if (/*chr==khe_Joined_AtEnd ||*/ chr == khe_Joined_AtMidle) return khe_Joined_AtStart;

      if (/*chr==seen_Joined_AtEnd ||*/ chr == seen_Joined_AtMidle) return seen_Joined_AtStart;

      if (/*chr==sheen_Joined_AtEnd ||*/ chr == sheen_Joined_AtMidle) return sheen_Joined_AtStart;

      if (/*chr==swad_Joined_AtEnd ||*/ chr == swad_Joined_AtMidle) return swad_Joined_AtStart;

      if (/*chr==dwad_Joined_AtEnd ||*/ chr == dwad_Joined_AtMidle) return dwad_Joined_AtStart;

      if (/*chr==toe_Joined_AtEnd ||*/ chr == toe_Joined_AtMidle) return toe_Joined_AtStart;

      if (/*chr==zoe_Joined_AtEnd ||*/ chr == zoe_Joined_AtMidle) return zoe_Joined_AtStart;

      if (/*chr==an_Joined_AtEnd ||*/ chr == an_Joined_AtMidle) return an_Joined_AtStart;

      if (/*chr==ghain_Joined_AtEnd ||*/ chr == ghain_Joined_AtMidle) return ghain_Joined_AtStart;

      if (/*chr==fe_Joined_AtEnd ||*/ chr == fe_Joined_AtMidle) return fe_Joined_AtStart;

      if (/*chr==kaaf_Joined_AtEnd ||*/ chr == kaaf_Joined_AtMidle) return kaaf_Joined_AtStart;

      if (/*chr==kiaf_Joined_AtEnd ||*/ chr == kiaf_Joined_AtMidle) return kiaf_Joined_AtStart;

      if (/*chr==gaaf_Joined_AtEnd ||*/ chr == gaaf_Joined_AtMidle) return gaaf_Joined_AtStart;

      if (/*chr==laam_Joined_AtEnd ||*/ chr == laam_Joined_AtMidle) return laam_Joined_AtStart;

      if (/*chr==meem_Joined_AtEnd ||*/ chr == meem_Joined_AtMidle) return meem_Joined_AtStart;

      if (/*chr==noon_Joined_AtEnd ||*/ chr == noon_Joined_AtMidle) return noon_Joined_AtStart;

      if (/*chr==chotiye_Joined_AtEnd || */chr == hamza_Joined_AtMidle) return hamza_Joined_AtStart;

      if (/*chr==chotiye_Joined_AtEnd ||*/ chr == chotiye_Joined_AtMidle) return chotiye_Joined_AtStart;
      return chr;
    }

  }
}
